/* Nama Program : fupah.c                                 *
 * NPM          : …                                       *
 * Nama         : …                                       *
 * Tgl buat     : 16-10-2024                              *
 * Deskripsi    : Menentukan upah karyawan                * 
 *                berdasarkan golongan dan tunjangan anak *
 * ------------------------------------------------------ */                  

#include <stdio.h>
#include <ctype.h> // for tolower()

/* Function to input employee details */
void input(char *Nama, char *gol, int *jmlanak) {
    printf("Masukkan Nama Karyawan: ");
    scanf(" %[^\n]", Nama); // to read string with spaces
    printf("Masukkan Golongan (A/B/C): ");
    scanf(" %c", gol);
    printf("Masukkan Jumlah Anak: ");
    scanf("%d", jmlanak);
}

/* Function to calculate gross salary (upah kotor) based on the employee's group */
float Upahkotor(char gol) {
    switch (tolower(gol)) {
        case 'a': return 3000000;
        case 'b': return 4000000;
        case 'c': return 5000000;
        default: return 0;
    }
}

/* Function to calculate child allowance percentage based on the number of children */
float Persentunjangan(int jmlanak) {
    if (jmlanak == 0)
        return 0.0;      // 0% for 0 children
    else if (jmlanak == 1)
        return 0.05;     // 5% for 1 child
    else if (jmlanak == 2)
        return 0.1;      // 10% for 2 children
    else
        return 0.15;     // 15% for more than 2 children
}

int main() {
    char Nama[100];       // to hold the employee's name
    char Gol;             // to hold the group (A, B, C)
    int jmlanak;          // to hold the number of children
    float upahbersih, upahkotor;

    /* Input employee details */
    input(Nama, &Gol, &jmlanak);

    /* Calculate gross salary and net salary */
    upahkotor = Upahkotor(Gol);
    upahbersih = upahkotor - (upahkotor * Persentunjangan(jmlanak));

    /* Output the net salary */
    printf("Upah Bersih    : Rp %.2f\n", upahbersih);

    return 0;
}
