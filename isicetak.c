/* Nama Program : isicetak.c                         *
 * NPM          : …                                  *
 * Nama         : …                                  *
 * Tgl buat     : 16-10-2024                         *
 * Deskripsi    : Mengisi dan mencetak array         *
 * ------------------------------------------------- */

#include <stdio.h>

int main() {
    int i;
    int nilai[3];  // Array to hold 3 integer values

    /* Mengisi elemen array */
    printf("Mengisi elemen array\n");
    for (i = 0; i < 3; i++) {
        printf("Nilai ke %d = ", i + 1);
        scanf("%d", &nilai[i]);
    }

    /* Mencetak elemen array */
    printf("Mencetak elemen array\n");
    for (i = 0; i < 3; i++) {
        printf("Nilai ke %d = %d\n", i + 1, nilai[i]);
    }

    return 0;
}
