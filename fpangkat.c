/* Nama Program : fpangkat.c                                    *
 * NPM          : …                                             * 
 * Nama         : …                                             *  
 * Tgl buat     : 15-10-2024                                    * 
 * Deskripsi    : Memangkatkan bilangan dengan pangkat tertentu *
 * ------------------------------------------------------------ */

#include <stdio.h>

/* Fungsi untuk menghitung hasil pangkat */
float fpangkat(float dipangkatkan, int pangkat) {
    float hasilpangkat = 1;  /* Inisialisasi hasil pangkat */
    int i;

    /* Loop untuk menghitung pangkat */
    for (i = 1; i <= pangkat; i++) {
        hasilpangkat *= dipangkatkan;
    }

    return hasilpangkat;  /* Mengembalikan hasil pangkat */
}

int main() {
    float dipangkatkan = 1;  /* Bilangan yang akan dipangkatkan */
    int pangkat = 1;          /* Pangkatnya */

    /* Input bilangan dan pangkat */
    printf("Bilangan yang akan dipangkatkan: ");
    scanf("%f", &dipangkatkan);

    printf("Masukkan pangkatnya: ");
    scanf("%d", &pangkat);

    /* Output hasil */
    printf("%.2f^%d : %.2f\n", dipangkatkan, pangkat, fpangkat(dipangkatkan, pangkat));

    return 0;
}
