/* Nama Program : bintang.c                        *
 * NPM          : …                                *
 * Nama         : …                                *
 * Tgl buat     : 13-10-2024                       *
 * Deskripsi    : Mencetak bintang                 *
 * ----------------------------------------------- */

#include <stdio.h>

int main() {
    int N = 0;        /* jumlah awal bintang */
    int i = 0, j = 0; /* counter untuk loop */

    /* Input jumlah awal bintang */
    printf("Masukkan jumlah awal bintang: ");
    scanf("%d", &N);

    /* Loop untuk mencetak bintang secara menurun */
    for (i = N; i >= 0; i--) {
        for (j = 1; j <= i; j++) {
            printf("*");
        }
        printf("\n");
    }

    return 0;
}
