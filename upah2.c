/* Nama Program : upah2.c                                 *
 * NPM          : …                                       *
 * Nama         : …                                       *
 * Tgl buat     : 16-10-2024                              *
 * Deskripsi    : Menentukan upah karyawan berdasarkan    * 
 *                golongan dan tunjangan anak             *
 * ------------------------------------------------------ */

#include <stdio.h>
#include <ctype.h>  // For toupper() function

/* Define a structure to hold employee data */
typedef struct {
    char Nama[101];  // String for employee name (increased length to 100 characters)
    char Gol;        // Character for employee grade (A - D)
    int JmlAnak;     // Number of children
} Tpegawai;

int main() {
    Tpegawai pegawai;
    float Upahkotor = 0.0, Upahbersih = 0.0;
    float PersenTunjangan = 0.0;

    /* Input employee data */
    printf("Nama        : ");
    scanf(" %[^\n]", pegawai.Nama);  // This will allow reading a name with spaces until newline is encountered

    printf("Gol (A - D) : ");
    scanf(" %c", &pegawai.Gol);  // Note the space before %c to consume any whitespace
    pegawai.Gol = toupper(pegawai.Gol);  // Convert Gol to uppercase

    printf("Jumlah Anak : ");
    scanf("%d", &pegawai.JmlAnak);

    /* Determine gross salary based on employee grade */
    switch (pegawai.Gol) {
        case 'A':
            Upahkotor = 1000000;
            break;
        case 'B':
            Upahkotor = 800000;
            break;
        case 'C':
            Upahkotor = 600000;
            break;
        case 'D':
            Upahkotor = 400000;
            break;
        default:
            printf("Golongan tidak valid!\n");
            return 1;
    }

    /* Determine percentage of allowance based on number of children */
    if (pegawai.JmlAnak > 2)
        PersenTunjangan = 0.3;
    else
        PersenTunjangan = 0.2;

    /* Calculate net salary */
    Upahbersih = Upahkotor - (Upahkotor * PersenTunjangan);

    /* Output the net salary */
    printf("Upah        : Rp %.2f\n", Upahbersih);

    return 0;
}
