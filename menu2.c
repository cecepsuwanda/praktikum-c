/* Nama Program : menu2.c                                                                   *
 * NPM          : …                                                                         *
 * Nama         : …                                                                         *
 * Tgl buat     : 13-10-2024                                                                *
 * Deskripsi    : Membuat menu untuk menghitung isi kubus, luas lingkaran, dan isi silinder *
 * ---------------------------------------------------------------------------------------- */

#include <stdio.h>
#include <conio.h>

#define PI 3.14  /* konstanta pi */

int main() {
    int pilih = -1;         /* pilihan menu */
    float sisi = 0, jari = 0, tinggi = 0;  /* variabel untuk input */

    do {
        /* Menampilkan menu */
        printf("         <<< Menu >>>        \n\n");
        printf("1. Menghitung Isi Kubus\n");
        printf("2. Menghitung Luas Lingkaran\n");
        printf("3. Menghitung Isi Silinder\n");
        printf("0. Keluar Program\n");
        printf("Pilih Nomor : ");
        scanf("%d", &pilih);

        /* Switch-case untuk setiap pilihan menu */
        switch (pilih) {
            case 1:
                printf("Panjang Sisi Kubus : ");
                scanf("%f", &sisi);
                printf("Isi Kubus : %.2f\n", sisi * sisi * sisi);
                break;

            case 2:
                printf("Jari-jari Lingkaran : ");
                scanf("%f", &jari);
                printf("Luas Lingkaran : %.2f\n", PI * jari * jari);
                break;

            case 3:
                printf("Jari-jari Lingkaran : ");
                scanf("%f", &jari);
                printf("Tinggi Silinder : ");
                scanf("%f", &tinggi);
                printf("Isi Silinder : %.2f\n", PI * jari * jari * tinggi);
                break;

            case 0:
                printf("Keluar dari program.\n");
                break;

            default:
                printf("Pilihan tidak valid, coba lagi.\n");
        }

        getch();  /* Menunggu input dari pengguna sebelum kembali ke menu */
    } while (pilih != 0);  /* Ulangi sampai pengguna memilih 0 */

    return 0;
}
