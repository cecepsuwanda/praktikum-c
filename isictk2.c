/* Nama Program : isictk2.c                  *
 * NPM          : …                          *
 * Nama         : …                          *
 * Tgl buat     : 16-10-2024                 *
 * Deskripsi    : Mengisi dan mencetak array *
 * ----------------------------------------- */

#include <stdio.h>

/* Define the array type as an array of 3 integers */
typedef int Tarray[3];

/* Procedure to fill the array with values using a while loop */
void isi(Tarray nilai) {
    int i = 0;  // Initialize counter
    while (i < 3) {  // Use while loop to iterate over the array
        printf("Nilai ke %d = ", i + 1);
        scanf("%d", &nilai[i]);
        i++;  // Increment counter
    }
}

/* Procedure to print the values in the array */
void cetak(Tarray nilai) {
    int i;
    for (i = 0; i < 3; i++) {
        printf("Nilai ke %d = %d\n", i + 1, nilai[i]);
    }
}

int main() {
    Tarray nilai;  // Declare the array

    printf("Mengisi elemen array\n");
    isi(nilai);  // Call the isi function to fill the array

    printf("Mencetak elemen array\n");
    cetak(nilai);  // Call the cetak function to print the array

    return 0;
}
