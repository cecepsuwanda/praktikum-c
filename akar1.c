/* Nama Program : akar1.c                                 *
 * NPM          : …                                       *
 * Nama         : …                                       *
 * Tgl buat     : 10-10-2024                              *
 * Deskripsi    : menentukan jenis akar persamaan kuadrat *
 * ------------------------------------------------------ */
#include <stdio.h>

int main()
{
  float a=0.0,b=0.0,c=0.0,D=0.0;

  printf("Menentukan Jenis Akar Persamaan Kuadrat\n");
  printf("      Persamaan Umum : ax^2+bx+c       \n\n");

  printf("a = ");scanf("%f",&a);
  printf("b = ");scanf("%f",&b);
  printf("c = ");scanf("%f",&c);
  printf("\n");

  D=(b*b)-(4*a*c);
  printf("Nilai Diskriminan : %.2f\n",D);
  printf("Jenis Akar : ");
  if (D==0)
     printf("Kembar\n");
  else
    if (D>0)
       printf("Berlainan\n");
    else
       if (D<0)
          printf("imajiner berlainan\n");

return 0;
}
