/* Nama Program : masukabj.c                           *
 * NPM          : …                                    *
 * Nama         : …                                    *
 * Tgl buat     : 10-10-2024                           *
 * Deskripsi    : memeriksa inputan                    *
 * --------------------------------------------------- */

#include <stdio.h>

int main()
{
  char A = '\0';

  printf("Masukkan Suatu Karakter : ");scanf("%c",&A);

  if ((A>='A') && (A<='Z'))
    {
     printf("Anda menekan huruf besar\n");
     printf("Huruf yang anda tekan, Huruf %c\n",A);
    }
  else
    {
     printf("Anda tidak menekan huruf besar\n");
     printf("Karakter yang anda tekan, Karakter %c\n",A);
    }

return 0;
}