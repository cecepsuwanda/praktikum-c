/* Nama Program : upah.c                                                           *
 * NPM          : …                                                                *
 * Nama         : …                                                                *
 * Tgl buat     : 10-10-2024                                                       *
 * Deskripsi    : menentukan upah karyawan berdasarkan golongan dan tunjangan anak *
 * ------------------------------------------------------------------------------- */

#include <stdio.h>

int main()
{
  char Nama[25] = "\0";
  char Gol = '\0';
  int JmlAnak = 0;
  float Upahkotor = 0.0,upahbersih = 0.0;
  float PersenTunjangan = 0.0;

  printf("Nama        : ");scanf("%s",Nama);
  printf("Gol (A - D) : ");scanf(" %c",&Gol);
  printf("Jumlah Anak : ");scanf("%d",&JmlAnak);

  switch (Gol)
  {
    case 'A' : Upahkotor=1000000;break;
    case 'B' : Upahkotor=800000;break;
    case 'C' : Upahkotor=600000;break;
    case 'D' : Upahkotor=400000;break;
  }

  if(JmlAnak>2)
    PersenTunjangan = 0.3;
  else
    PersenTunjangan = 0.2;

  upahbersih = Upahkotor - (Upahkotor * PersenTunjangan);
  printf("Upah        : %.2f\n",upahbersih);

return 0;
}
