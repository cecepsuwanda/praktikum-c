/* Nama Program : tunja1.c                             *
 * NPM          : …                                    *
 * Nama         : …                                    *
 * Tgl buat     : 10-10-20214                          *
 * Deskripsi    : menghitung besarnya jumlah tunjangan *
 * --------------------------------------------------- */

#include <stdio.h>

int main()
{
    int JumlahAnak = 0;
    float GajiKotor = 0.0,Tunjangan = 0.0,PersenTunjangan = 0.0;

    PersenTunjangan = 0.2;
    printf("Gaji Kotor ? ");scanf("%f",&GajiKotor);
    printf("Jumlah Anak ? ");scanf("%d",&JumlahAnak);
    if (JumlahAnak>2)
       PersenTunjangan=0.3;
    Tunjangan = PersenTunjangan*GajiKotor;
    printf("Besar Tunjangan = Rp %10.2f",Tunjangan);

return 0;
}
