# Praktikum C

Project ini berisi file-file source code dalam bahasa C

Urutan baca contoh code :

runtunan :

  1. hello1.c
  2. hello2.c
  3. assign.c
  4. baca.c
  5. operator.c
  6. latihan_1.text

pemilihan :

  7. tunja1.c
  8. tunja2.c
  9. masuka.c
  10. masukabj.c
  11. akar1.c
  12. akar2.c
  13. upah.c
  14. menu.c
  15. latihan_2.text

perulangan :

  16. pangkat.c
  17. cdown.c
  18. rata2.c
  19. bintang.c
  20. tabel.c
  21. menu2.c
  22. latihan_3.text

prosedure & fungsi :

  23. fpangkat.c
  24. faktorial.c
  25. ftabel.c
  26. fupah.c
  27. latihan_4.text

array/larik :

  28. isicetak.c
  29. isictk2.c
  30. matrik.c

record/struct :

  31. upah2.c
  32. fupah2.c
  33. fupah3.c

pengurutan :

  34. sort1.c
  35. sort2.c
  36. sort3.c
  37. sort4.c


