/* Nama Program : tabel.c                                           *
 * NPM          : …                                                 *
 * Nama         : …                                                 *
 * Tgl buat     : 13-10-2024                                                 *
 * Deskripsi    : Program untuk mencetak tabel x, 1/x, x^2, dan x^3 *
 * ---------------------------------------------------------------- */

#include <stdio.h>

int main() {
    int x = 1, i = 0, pangkat = 0, hasilpangkat = 1;

    /* Mencetak header tabel */
    printf("  x      1/x        x^2      x^3\n");
    printf("-----------------------------------\n");

    /* Loop untuk mencetak nilai x dari 1 sampai 10 */
    while (x <= 10) {
        /* Mencetak nilai x dan 1/x */
        printf("%5d%10.5f", x, 1.0 / x);

        /* Inisialisasi pangkat mulai dari 2 */
        pangkat = 2;

        /* Loop untuk menghitung dan mencetak x^2 dan x^3 menggunakan while */
        while (pangkat <= 3) {
            hasilpangkat = 1;

            /* Loop for untuk menghitung pangkat */
            for (i = 1; i <= pangkat; i++) {
                hasilpangkat *= x;
            }

            printf("%8d", hasilpangkat); /* Mencetak hasil pangkat */
            pangkat++;
        }

        printf("\n");  /* Pindah ke baris berikutnya */
        x++;           /* Naikkan nilai x */
    }

    return 0;
}
