/* Nama Program : cdown.c                                *
 * NPM          : …                                      *
 * Nama         : …                                      *
 * Tgl buat     : 12-10-2024                             *
 * Deskripsi    : Menghitung mundur N, N-1, N-2, …, 1, 0 *
 * ----------------------------------------------------- */

#include <stdio.h>

int main() {
    int N=0, i=0;

    // Input angka awal
    printf("Masukkan angka: ");
    scanf("%d", &N);

    // Loop menghitung mundur dari N ke 1
    for (i = N; i >= 1; i--) {
        printf("%d, ", i);
    }

    // Output akhir 0
    printf("0\n");

    return 0;
}
