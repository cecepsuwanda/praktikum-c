/* Nama Program : fupah3.c                                 *
 * NPM          : …                                       *
 * Nama         : …                                       *
 * Tgl buat     : 16-10-2024                              *
 * Deskripsi    : Menentukan upah karyawan                * 
 *                berdasarkan golongan dan tunjangan anak *
 * ------------------------------------------------------ */       

#include <stdio.h>
#include <ctype.h>  // For toupper() function

/* Define a structure to hold employee data */
typedef struct {
    char Nama[21];  // String for employee name (20 characters max + null terminator)
    char Gol;       // Character for employee grade (A - D)
    int JmlAnak;    // Number of children
} Tpegawai;

/* Define an array of 5 employees */
typedef Tpegawai Tdaf_peg[5];

/* Function to input employee data */
void input(Tpegawai *pegawai) {
    printf("Nama        : ");
    scanf(" %[^\n]", pegawai->Nama);  // Reads a name with spaces until newline

    printf("Gol (A - D) : ");
    scanf(" %c", &pegawai->Gol);  // Reads the employee's grade
    pegawai->Gol = toupper(pegawai->Gol);  // Convert Gol to uppercase

    printf("Jumlah Anak : ");
    scanf("%d", &pegawai->JmlAnak);  // Reads the number of children
}

/* Function to calculate gross salary based on employee's grade */
float upahkotor(char gol) {
    switch (gol) {
        case 'A':
            return 1000000;
        case 'B':
            return 800000;
        case 'C':
            return 600000;
        case 'D':
            return 400000;
        default:
            return 0;  // Default case for invalid input
    }
}

/* Function to calculate the allowance percentage based on the number of children */
float persentunjangan(int jmlanak) {
    if (jmlanak > 2) {
        return 0.3;  // 30% allowance for more than 2 children
    } else {
        return 0.2;  // 20% allowance for 2 or fewer children
    }
}

/* Function to print employee details and calculate net salary */
void cetak(Tpegawai pegawai) {
    float upahKotor = upahkotor(pegawai.Gol);
    float tunjangan = upahKotor * persentunjangan(pegawai.JmlAnak);
    float upahBersih = upahKotor - tunjangan;

    printf("\nNama        : %s\n", pegawai.Nama);
    printf("Gol (A - D) : %c\n", pegawai.Gol);
    printf("Jumlah Anak : %d\n", pegawai.JmlAnak);
    printf("Upah        : Rp %.2f\n", upahBersih);
}

int main() {
    Tdaf_peg daf_pegawai;  // Array of 5 employees
    int i;

    /* Input data for 5 employees */
    for (i = 0; i < 5; i++) {
        printf("Data ke %d:\n", i + 1);
        input(&daf_pegawai[i]);
    }

    /* Print data and calculate net salary for 5 employees */
    for (i = 0; i < 5; i++) {
        printf("\nData ke %d:\n", i + 1);
        cetak(daf_pegawai[i]);
    }

    return 0;
}
