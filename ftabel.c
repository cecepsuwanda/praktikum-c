/* Nama Program : ftabel.c                                             *
 * NPM          : …                                                    *
 * Nama         : …                                                    *
 * Tgl buat     : 16-10-2024                                           *
 * Deskripsi    : Program ini membuat tabel yang berisi                * 
 *                nilai x, 1/x, x^2, dan x^3 dari masukan jumlah data. *
 * ------------------------------------------------------------------- */

#include <stdio.h>

/* Fungsi untuk menghitung hasil pangkat */
float fpangkat(float dipangkatkan, int pangkat) {
    float hasil = 1;
    int i;

    /* Loop untuk menghitung pangkat */
    for (i = 1; i <= pangkat; i++) {
        hasil *= dipangkatkan;
    }

    return hasil;  /* Mengembalikan hasil pangkat */
}

/* Prosedur untuk mencetak tabel */
void cetak(int n) {
    int i;
    float satuPerX, x2, x3;

    /* Cetak header tabel */
    printf("  x      1/x        x^2        x^3\n");
    printf("-----------------------------------\n");

    /* Loop untuk mencetak nilai x, 1/x, x^2, dan x^3 */
    for (i = 1; i <= n; i++) {
        satuPerX = 1.0f / i;
        x2 = fpangkat((float)i, 2);
        x3 = fpangkat((float)i, 3);
        printf("%4d  %10.4f  %10.4f  %10.4f\n", i, satuPerX, x2, x3);
    }
}

int main() {
    int x;

    /* Input jumlah data */
    printf("Masukkan banyaknya data : ");
    scanf("%d", &x);

    /* Panggil prosedur cetak untuk menampilkan tabel */
    cetak(x);

    return 0;
}
