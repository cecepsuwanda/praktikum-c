/* Nama Program : assign.c                             *
 * NPM          : …                                    *
 * Nama         : …                                    *
 * Tgl buat     : 09-10-2024                           *
 * Deskripsi    : assigment dan cetak dengan format    *
 * --------------------------------------------------- */

#include <stdio.h>
#include <string.h>

int main()
{
  int i = 0;
  float l = 0.0;
  char Shello[16]="\0";

  strcpy(Shello, "Hello, dunia !!!");
  i=-1234;
  l=3.14;
  printf("%s \n",Shello);
  printf("Nilai i (integer)= %5d\n",i);
  printf("Nilai l (real)   = %5.2f\n",l);

  return 0;
}
