/* Nama Program : rata2.c                         *
 * NPM          : …                               *
 * Nama         : …                               *
 * Tgl buat     : 13-10-2024                      *
 * Deskripsi    : Menghitung rata-rata            *
 * ---------------------------------------------- */

#include <stdio.h>

int main() {
    int i=0, N=0;              /* counter dan jumlah data */
    float Data=0.0, Rata=0.0, Total = 0;  /* variabel untuk data, rata-rata, dan total */

    /* Input jumlah data */
    printf("Banyaknya data: ");
    scanf("%d", &N);

    /* Loop untuk input data dan menghitung total */
    for (i = 1; i <= N; i++) {
        printf("Data ke-%d: ", i);
        scanf("%f", &Data);
        Total += Data;  /* Menambahkan data ke total */
    }

    /* Menghitung rata-rata */
    Rata = Total / N;

    /* Output hasil */
    printf("Banyaknya Data       : %d\n", N);
    printf("Total Nilai Data     : %.2f\n", Total);
    printf("Rata-rata nilai Data : %.2f\n", Rata);

    return 0;
}
