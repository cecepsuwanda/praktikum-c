// Nama Program : sort3.c
// NPM          : …
// Nama         : …
// Tgl buat     : 24-10-2024
// Deskripsi    : Selection Sort 


#include <stdio.h>
#include <stdlib.h>  // For rand() and srand()
#include <time.h>    // For time()

// Function to swap two elements
void swap(int *a, int *b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}

// Function to perform Selection Sort
void selectionSort(int arr[], int n) {
    for (int i = 0; i < n - 1; i++) {
        // Assume the minimum is the first element
        int minIndex = i;

        // Find the index of the minimum element in the unsorted part
        for (int j = i + 1; j < n; j++) {
            if (arr[j] < arr[minIndex]) {
                minIndex = j;
            }
        }

        // Swap the found minimum element with the first element of the unsorted part
        if (minIndex != i) {
            swap(&arr[i], &arr[minIndex]);
        }
    }
}

// Function to display the array
void printArray(int arr[], int size) {
    for (int i = 0; i < size; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

int main() {
    int arr[30];

    // Seed the random number generator
    srand(time(0));

    // Assign 30 random numbers between 1 and 100 to the array
    for (int i = 0; i < 30; i++) {
        arr[i] = rand() % 100 + 1;  // Random number between 1 and 100
    }

    // Display the original array
    printf("Original array:\n");
    printArray(arr, 30);

    // Perform Selection Sort
    selectionSort(arr, 30);

    // Display the sorted array
    printf("Sorted array:\n");
    printArray(arr, 30);

    return 0;
}
