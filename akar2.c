/* Nama Program : akar2.c                              *
 * NPM          : …                                    *
 * Nama         : …                                    *
 * Tgl buat     : 10-10-2024                           *
 * Deskripsi    : mencari akar persamaan kuadrat       *
 * --------------------------------------------------- */
#include <stdio.h>

int main()
{
  float a=0.0,b=0.0,c=0.0,D=0.0,x1=0.0,x2=0.0;

  printf("Menentukan Jenis Akar Persamaan Kuadrat\n");
  printf("      Persamaan Umum : ax^2+bx+c       \n\n");

  printf("a = ");scanf("%f",&a);
  printf("b = ");scanf("%f",&b);
  printf("c = ");scanf("%f",&c);
  printf("\n");

  D=(b*b)-(4*a*c);
  printf("Nilai Diskriminan : %.2f\n",D);
  printf("Jenis Akar : ");
  if (D==0)
    {
     x1=(-b)/(2*a);
     printf("Kembar\n");
     printf("x1=x2=%10.2f\n",x1);
    }
  else
    if (D>0)
      {
        x1=((-b)+ sqrt(D))/(2*a);
        x2=((-b)+ sqrt(D))/(2*a);
        printf("Berlainan\n");
        printf("x1 = %10.2f\n",x1);
        printf("x2 = %10.2f\n",x2);
      }
    else
    if (D<0)
      {
        x1=(-b)/(2*a);
        x2=sqrt(-D)/(2*a);
        printf("2 akar imajiner berlainan\n");
        printf("x1 = %5.2f + %5.2f i\n",x1,x2);
        printf("x2 = %5.2f - %5.2f i\n",x1,x2);
      }

return 0;
}
