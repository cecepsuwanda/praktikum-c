/* Nama Program : operator.c                           *
 * NPM         : …                                     *
 * Nama        : …                                     *
 * Tgl buat    : 09-10-2024                            *
 * Deskripsi   : pengoperasian variabel bertipe dasar  *
 * --------------------------------------------------- */

#include <stdio.h>
#include <stdbool.h>
#include <string.h>


int main()
{
  bool TF = true;
  int i = 0,j = 0,hsl = 0;
  float x = 0.0,y = 0.0;
  int hsl1 = 0;
  float hsl2 = 0.0;


  /* proses boolean */
  printf("tabel kebenaran\n");
  printf("-----------------------------------------------------\n");
  printf("|   x1  |   x2  |     x1 and x2     |    x1 or x2  |\n");
  printf("----------------------------------------------------\n");
  printf("| true  | true  |         %d         |      %d       |\n",(true && true),(true || true));
  printf("| true  | false |         %d         |      %d       |\n",(true && false),(true || false));
  printf("| false | true  |         %d         |      %d       |\n",(false && true),(false || true));
  printf("| false | false |         %d         |      %d       |\n",(false && false),(false || false));
  printf("----------------------------------------------------\n");

  /* proses operasi numerik */
  i=5;j=2;
  printf("operasi numerik pada tipe data integer\n");
  printf("---------------------------\n");
  printf("| operasi | hasil operasi |\n");
  printf("---------------------------\n");
  printf("| %d + %d   |    %6d     |\n",i,j,i+j);
  printf("| %d - %d   |    %6d     |\n",i,j,i-j);
  printf("| %d * %d   |    %6d     |\n",i,j,i*j);
  printf("| %d / %d   |    %6.2f     |\n",i,j,(float)i/j);
  printf("| %d div %d |    %6d     |\n",i,j,i/j);
  printf("| %d mod %d |    %6d     |\n",i,j,i%j);
  printf("---------------------------\n");
  

  x=5.0;y=2.0;
  printf("operasi numerik pada tipe data float\n");
  printf("---------------------------\n");
  printf("| operasi | hasil operasi |\n");
  printf("---------------------------\n");
  printf("| %3.1f + %3.1f   | %6.1f    |\n",x,y,x+y);
  printf("| %3.1f - %3.1f   | %6.1f    |\n",x,y,x-y);
  printf("| %3.1f * %3.1f   | %6.1f    |\n",x,y,x*y);
  printf("| %3.1f / %3.1f   | %6.1f    |\n",x,y,x/y);
  printf("---------------------------\n");

 /* operator relasi numerik */
 TF = x!=y;
 printf("5.0 <> 2.0 adalah= %d\n",TF);
 TF = x<y;
 printf("5.0 < 2.0 adalah= %d\n",TF);

 return 0;
}