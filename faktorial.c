/* Nama Program : faktorial.c                   *
 * NPM          : …                             * 
 * Nama         : …                             *  
 * Tgl buat     : 15-10-2024                    * 
 * Deskripsi    : Menghitung n! secara rekursif *
 * -------------------------------------------- */

#include <stdio.h>

/* Fungsi rekursif untuk menghitung faktorial */
int fakt(int n) {
    if (n == 0)
        return 1;  /* Basis rekursi: 0! = 1 */
    else
        return n * fakt(n - 1);  /* Rekursi: n * (n-1)! */
}

int main() {
    int n;  /* Variabel untuk menyimpan input */

    /* Input nilai n */
    printf("Masukkan n : ");
    scanf("%d", &n);

    /* Output hasil faktorial */
    printf("%d! = %d\n", n, fakt(n));

    return 0;
}
