/* Nama Program : matrik.c                    *
 * NPM          : …                           *
 * Nama         : …                           *
 * Tgl buat     : 16-10-2024                  *
 * Deskripsi    : Mengisi dan mencetak matrik *
 * ------------------------------------------ */

#include <stdio.h>

/* Define the matrix type as a 3x3 array of integers */
typedef int tmatrik[3][3];

/* Procedure to fill the matrix with values */
void isi(tmatrik matrik) {
    int i, j;
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            printf("A[%d,%d] = ", i + 1, j + 1); // User-friendly index
            scanf("%d", &matrik[i][j]);
        }
    }
}

/* Procedure to print the values in the matrix */
void cetak(tmatrik matrik) {
    int i, j;
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            printf("%3d", matrik[i][j]); // Right-aligned output
        }
        printf("\n"); // New line after each row
    }
}

int main() {
    tmatrik matrik;  // Declare the matrix

    printf("Mengisi elemen matrik A\n");
    isi(matrik);  // Call the isi function to fill the matrix

    printf("Mencetak elemen matrik A\n");
    cetak(matrik);  // Call the cetak function to print the matrix

    return 0;
}
