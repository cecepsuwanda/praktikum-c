/* Nama Program : pangkat.c                                      *
 * NPM          : …                                              *
 * Nama         : …                                              *
 * Tgl buat     : 12-10-2024                                     *
 * Deskripsi    : Memangkatkan bilangan dengan pangkat tertentu  *
 * ------------------------------------------------------------- */

#include <stdio.h>

int main() {
    int i=0, pangkat=0;
    float hasilpangkat=0.0, dipangkatkan=0.0;

    printf("Bilangan yang akan dipangkatkan: ");
    scanf("%f", &dipangkatkan);

    printf("Masukkan pangkatnya: ");
    scanf("%d", &pangkat);

    hasilpangkat = 1;
    for (i = 1; i <= pangkat; i++) {
        hasilpangkat *= dipangkatkan;
    }

    printf("%.2f^%d : %.2f\n", dipangkatkan, pangkat, hasilpangkat);

    return 0;
}
