/* Nama Program : baca.c                               *
 * NPM         : …                                     *
 * Nama        : …                                     *
 * Tgl buat    : 09-10-2024                            *
 * Deskripsi   : contoh membaca nilai dan kemudian     *
 *               menuliskan nilai yang dibaca          *
 * --------------------------------------------------- */

#include <stdio.h>
#include <string.h>


int main()
{
  int i = 0;
  float j = 0.0;
  char kar = '\0';
  char NString[20] = "\0";

  printf("Contoh membaca dan menulis\n");
  printf("Masukkan nilai integer  : ");scanf("%d",&i);
  printf("Masukkan nilai real     : ");scanf("%f",&j);
  printf("Masukkan nilai karakter : ");scanf(" %c",&kar);
  printf("Masukkan nilai string   : ");scanf("%s",NString);
  printf("Nilai integer yang dibaca  =%d\n",i);
  printf("Nilai real yang dibaca     =%f\n",j);
  printf("Nilai karakter yang dibaca =%c\n",kar);
  printf("Nilai string yang dibaca   =%s\n",NString);

  return 0;
}